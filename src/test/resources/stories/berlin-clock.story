Story: The Berlin Clock

Narrative:
    As a clock enthusiast
    I want to tell the time using the Berlin Clock
    So that I can increase the number of ways that I can read the time

Scenario: Midnight 12 hours format
When The time is 00:00:00
Then The clock should show
Y
OOOO
OOOO
OOOOOOOOOOO
OOOO

Scenario: Midday
When The time is 13:17:01
Then The clock should show
O
RROO
RRRO
YYROOOOOOOO
YYOO

Scenario: Before midnight
When The time is 23:59:59
Then The clock should show
O
RRRR
RRRO
YYRYYRYYRYY
YYYY

Scenario: Midnight 24 hours format
When The time is 24:00:00
Then The clock should show
Y
RRRR
RRRR
OOOOOOOOOOO
OOOO

Scenario: Clock with wrong lights for seconds
When The clock shows
R
RRRO
RROO
YYROOOOOOOO
OOOO
Then I should detect an error

Scenario: Clock with wrong lights for minutes
When The clock shows
Y
RRRO
RROO
YYYOOOOOOOO
OOOO
Then I should detect an error

Scenario: Clock with wrong lights for hours
When The clock shows
Y
RRRO
RRYO
YYROOOOOOOO
OOOO
Then I should detect an error

Scenario: Clock with even seconds
When The time is <time>
Then It should be <light>

Examples:
|time|light|
|12:45:22|Y|
|17:25:25|O|