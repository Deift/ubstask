package com.ubs.opsit.interviews.model;

import com.ubs.opsit.interviews.*;
import sun.jvm.hotspot.utilities.Assert;


public class BerlinClockModel {

    private String topLight;
    private String row1Hour;
    private String row2Hour;
    private String row1Minutes;
    private String row2Minutes;

    public String getTopLight() {
        return topLight;
    }

    public void setTopLight(String topLight) {
        this.topLight = topLight;
    }

    public void checkTopLight() {
        Assert.that(this.topLight.length() == 1, "Top light should have only 1 element");
        Assert.that(!this.topLight.matches("(.*)[^YO](.*)"), "Top light should have only Y or/and O");
    }

    public String getRow1Hour() {
        return row1Hour;
    }

    public void setRow1Hour(String row1Hour) {
        this.row1Hour = row1Hour;
    }

    public void checkRow1Hour() {
        Assert.that(this.row1Hour.length() == 4, "First row of lights for hours should have only 4 elements");
        Assert.that(!this.row1Hour.matches("(.*)[^RO](.*)"), "First row of lights for hours should have only R or/and O");
        Assert.that(!this.row1Hour.matches("(.*)[O][R](.*)"), "First row of lights for hours shouldn't have OR order");
    }

    public String getRow2Hour() {
        return row2Hour;
    }

    public void setRow2Hour(String row2Hour) {
        this.row2Hour = row2Hour;
    }

    public void checkRow2Hour() {
        Assert.that(this.row2Hour.length() == 4, "Second row of lights for hours should have only 4 elements");
        Assert.that(!this.row2Hour.matches("(.*)[^RO](.*)"), "First row of lights for hours should have only R or/and O");
        Assert.that(!this.row2Hour.matches("(.*)[O][R](.*)"), "First row of lights for hours shouldn't have OR order");
    }

    public String getRow1Minutes() {
        return row1Minutes;
    }

    public void setRow1Minutes(String row1Minutes) {
        this.row1Minutes = row1Minutes;
    }

    public void checkRow1Minutes() {
        Assert.that(this.row1Minutes.length() == 11, "First row of lights for minutes should have only 11 elements");
        Assert.that(!this.row1Minutes.matches("(.*)[^RYO](.*)"), "First row of lights for minutes should have only R,Y and O elements");
        Assert.that(!this.row1Minutes.matches("(.*)[Y]{3,}(.*)"), "First row of lights for minutes should have a maximum of 2 Y together");
    }

    public String getRow2Minutes() {
        return row2Minutes;
    }

    public void setRow2Minutes(String row2Minutes) {
        this.row2Minutes = row2Minutes;
    }

    public void checkRow2Minutes() {
        Assert.that(this.row2Minutes.length() == 4, "Second row of lights for minutes should have only one element");
        Assert.that(!this.row2Minutes.matches("(.*)[^YO](.*)"), "Second row of lights for minutes should have only Y or/and O");
        Assert.that(!this.row2Minutes.matches("(.*)[O][Y](.*)"), "Second row of lights for minutes shouldn't have OY order");
    }

    public void setAllLights(String[] lights) {
        this.setTopLight(lights[0]);
        this.setRow1Hour(lights[1]);
        this.setRow2Hour(lights[2]);
        this.setRow1Minutes(lights[3]);
        this.setRow2Minutes(lights[4]);
    }

    public void checkAllLights() {
        this.checkTopLight();
        this.checkRow1Hour();
        this.checkRow2Hour();
        this.checkRow1Minutes();
        this.checkRow2Minutes();
    }

    private TimeConverter converter = new BerlinClockTimeConverter();

    public String getRealTime() {
        return this.converter.convertTime(this.getTopLight() + this.getRow1Hour() + this.getRow2Hour() +
                this.getRow1Minutes() + this.getRow2Minutes());
    }

}
