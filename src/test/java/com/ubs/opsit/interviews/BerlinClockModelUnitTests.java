package com.ubs.opsit.interviews;


import com.ubs.opsit.interviews.model.BerlinClockModel;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class BerlinClockModelUnitTests {

    private TimeConverter converter = new BerlinClockTimeConverter();
    private BerlinClockModel myModel = new BerlinClockModel();

    @Test
    public void assertTime() {
        String ligthsSample = "Y\nRRRO\nRROO\nYYROOOOOOOO\nYOOO";
        String realTimeSample = "17:16";
        String time = converter.convertTime(ligthsSample);
        assertThat(time, is(equalTo(realTimeSample)));
    }

    @Test
    public void assertTime2() {
        String ligthsSample = "Y\nRRRR\nRROO\nYYRYYRYOOOO\nYYYO";
        String realTimeSample = "22:38";
        String time = converter.convertTime(ligthsSample);
        assertThat(time, is(equalTo(realTimeSample)));
    }

    @Test
    public void assertTime3() {
        String ligthsSample = "Y\nROOO\nOOOO\nYYRYYRYYRYO\nOOOO";
        String realTimeSample = "05:50";
        String time = converter.convertTime(ligthsSample);
        assertThat(time, is(equalTo(realTimeSample)));
    }

}
