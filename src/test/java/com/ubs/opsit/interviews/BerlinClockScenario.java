package com.ubs.opsit.interviews;

import com.ubs.opsit.interviews.model.BerlinClockModel;
import com.ubs.opsit.interviews.model.Constants;
import org.jbehave.core.annotations.Named;
import org.junit.Test;


import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static com.ubs.opsit.interviews.support.BehaviouralTestEmbedder.aBehaviouralTestRunner;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class BerlinClockScenario {

    private String clockTime;
    private String lights;
    private BerlinClockModel clockModel = new BerlinClockModel();
    private TimeConverter converter = new BerlinClockTimeConverter();
    private String seconds;

    @Test
    public void berlinClockAcceptanceTests() throws Exception {
        aBehaviouralTestRunner()
                .usingStepsFrom(this)
                .withStory("berlin-clock.story")
                .run();
    }

    @When("The time is $")
    public void expectedRealTimeIs(String expectedTime) {
        this.clockTime = expectedTime;
    }

    @Then("The clock should show $")
    public void berlinClockLightsAre(String time) {
        String hours = this.converter.convertTime(time).split(Constants.timeSeparator)[0];
        String minutes = this.converter.convertTime(time).split(Constants.timeSeparator)[1];
        assertThat(hours, is(equalTo(this.clockTime.split(Constants.timeSeparator)[0])));
        assertThat(minutes, is(equalTo(this.clockTime.split(Constants.timeSeparator)[1])));
    }

    @When("The clock shows $")
    public void lightsAre(String lights) {
        this.lights = lights;
        this.clockModel.setAllLights(lights.split(Constants.lineSeparator));
    }

    @Then("I should detect an error")
    public void checkLigthsErrors() {
        this.clockModel.setAllLights(this.lights.split(Constants.lineSeparator));
        this.clockModel.checkAllLights();
    }

    @When("The time is <time>")
    public void timeIs(@Named("time") String time) {
        this.clockTime = time;
    }

    @Then("It should be <light>")
    public void checkSeconds(@Named("light") String light) {
        String secondsLight;
        if (Integer.parseInt(this.clockTime.split(Constants.timeSeparator)[2]) % 2 == 0) {
            secondsLight = Constants.yellowLight;
        } else {
            secondsLight = Constants.noLight;
        }
        assertThat(secondsLight, is(equalTo(light)));
    }

}
