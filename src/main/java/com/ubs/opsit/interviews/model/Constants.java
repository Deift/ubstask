package com.ubs.opsit.interviews.model;


public class Constants {

    public static final String noLight = "O";
    public static final String yellowLight = "Y";
    public static final String redLight = "R";
    public static final String lineSeparator = "\n";
    public static final String timeSeparator = ":";
    public static final String even = "even";
    public static final String odd = "odd";

}
