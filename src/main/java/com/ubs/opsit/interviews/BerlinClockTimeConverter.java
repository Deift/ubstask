package com.ubs.opsit.interviews;

import com.ubs.opsit.interviews.model.Constants;
import org.apache.commons.lang.StringUtils;


public class BerlinClockTimeConverter implements TimeConverter {

    @Override
    public String convertTime(String berlinClockTime) {
        String[] clockRows = berlinClockTime.split(Constants.lineSeparator);
        int hours = StringUtils.countMatches(clockRows[1], Constants.yellowLight) * 5 + StringUtils.countMatches(clockRows[1], Constants.redLight) * 5 +
                StringUtils.countMatches(clockRows[2], Constants.yellowLight) + StringUtils.countMatches(clockRows[2], Constants.redLight);
        int minutes = StringUtils.countMatches(clockRows[3], Constants.yellowLight) * 5 + StringUtils.countMatches(clockRows[3], Constants.redLight) * 5 +
                StringUtils.countMatches(clockRows[4], Constants.yellowLight) + StringUtils.countMatches(clockRows[4], Constants.redLight);
        return String.format("%02d", hours) + Constants.timeSeparator + String.format("%02d", minutes);
    }

}
